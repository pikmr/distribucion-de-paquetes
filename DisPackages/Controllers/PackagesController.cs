﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DisPackages.Entities;
using DisPackages.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DisPackages.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PackagesController : ControllerBase
    {
        private readonly IVehicleService _serviceVehicle;

        public PackagesController(IVehicleService serviceVehicle)
        {
            _serviceVehicle = serviceVehicle;
        }

        /// <summary>
        /// Obtiene la localización actual del vehículo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<Location>> GetLocation(int id)
        {
            var location=await _serviceVehicle.GetLocation(id);
            if (location != null)
                return NotFound($"Not exist Location id {id}");

            return location as Location;
        }

        [HttpPost]
        public async Task<ActionResult<IVehicle>> AddOrder(int IdVehiculo, Order newOrder)
        {
            var respond = await _serviceVehicle.InsertOrder(IdVehiculo, newOrder);
            if (respond != null)
                return NotFound($"Not exist id {IdVehiculo}");

            return Ok(respond);
        }

        /// <summary>
        /// Inserta una nueva localización al vehículo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<IVehicle>> DeleteOrder(int IdVehiculo, Order newOrder)
        {
            var respond = await _serviceVehicle.RemoveOrder(IdVehiculo, newOrder);
            if (respond != null)
                return NotFound($"Not exist id {IdVehiculo}");

            return Ok(respond);
        }

        /// <summary>
        /// Actualiza la localización del vehículo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<Location>> UpdateLocation(int id, Location location)
        {
            var respond = await _serviceVehicle.UpdateLocation(id, location);
            if (respond != null)
                return NotFound($"Not exist Location id {id}");

            return Ok(respond);
        }

        /// <summary>
        /// Actualiza la localización del vehículo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Location>> AddLocation(int id, Location location)
        {
            var respond = await _serviceVehicle.InsertLocation(id, location);
            if (respond != null)
                return NotFound($"Not exist Location id {id}");

            return Ok(respond);
        }

        /// <summary>
        /// Obtiene Información del packete así como del vehículo asignado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<Location>> GetInfoPackage(int id)
        {
            var package = _serviceVehicle.GetInfoPackage(id);
            return Ok(await package);
        }


    }
}
