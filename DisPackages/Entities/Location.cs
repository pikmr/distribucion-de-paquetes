﻿using DisPackages.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DisPackages.Entities
{
    public class Location : ILocation
    {
        public int CoordinateX { get; set; }
        public int CoordinateY { get; set; }
        public string Sector { get; set; }

        public Location()
        {
        }

        public void SetLocation(ILocation newlocation)
        {
            CoordinateX = newlocation.CoordinateX;
            CoordinateY = newlocation.CoordinateY;
        }

        public string GetDataLocation(IGeoLocationService serviceLocation)
        {
            return serviceLocation.GetDataLocation(this);
        }

        public override string ToString()
        {
            return base.ToString();
            //return GetDataLocation();
        }
    }
}
