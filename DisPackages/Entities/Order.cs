﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DisPackages.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public string Estado { get; set; }
        public List<Location> HistoryLocations { get; set; }
    }
}
