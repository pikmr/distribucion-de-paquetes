﻿using DisPackages.Services;

namespace DisPackages.Entities
{
    public interface ILocation
    {
        int CoordinateX { get; set; }
        int CoordinateY { get; set; }

        string GetDataLocation(IGeoLocationService serviceLocation);
        void SetLocation(ILocation newlocation);
    }
}