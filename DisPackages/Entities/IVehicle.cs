﻿namespace DisPackages.Entities
{
    public interface IVehicle
    {
        public Delivery Delivery { get; set; }
        void AddLocation(ILocation newlocation);
        void UpdateLocation(ILocation newlocation);
        ILocation GetActualLocation();
        void AddOrder(Order newOrder);
        void RemoveOrder(Order order);

    }
}