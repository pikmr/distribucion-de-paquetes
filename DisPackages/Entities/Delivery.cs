﻿using DisPackages.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DisPackages.Entities
{
    public class Delivery
    {
        public int Id { get; set; }
        private List<Order> Orders { get; set; }

        public Delivery()
        {
            Orders = new List<Order>();
        }

        public void AddOrder(Order newOrder)
        {
            if (Orders != null)
            {
                Orders.Add(newOrder);
            }
            else
            {
                Orders = new List<Order>();
                Orders.Add(newOrder);
            }
        }

        public void RemoveOrder(Order newOrder)
        {
            if (Orders != null && Orders.Any())
            {
                Orders.Remove(newOrder);
            }
        }

        public List<Order> GetCopyOrders()
        {
            return new List<Order>(Orders);
        }
    }
}
