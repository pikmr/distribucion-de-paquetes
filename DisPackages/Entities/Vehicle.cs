﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DisPackages.Entities
{
    public class Vehicle : IVehicle
    {
        public int Id { get; set; }
        private List<ILocation> HistoryLocations { get; }
        public Delivery Delivery { get; set; }

        public Vehicle()
        {
            HistoryLocations = new List<ILocation>();
            Delivery = new Delivery();
        }

        public void AddLocation(ILocation newlocation)
        {
            if (newlocation == null)
                throw new ArgumentNullException("Can't Agree a null Location.");

            HistoryLocations.Add(newlocation);
        }

        public void UpdateLocation(ILocation newlocation)
        {
            if (newlocation == null)
                throw new ArgumentNullException("Can't Agree a null Location.");

            GetActualLocation().SetLocation(newlocation);
        }

        public ILocation GetActualLocation()
        {
            return HistoryLocations.LastOrDefault();
        }

        public void AddOrder(Order newOrder)
        {
            Delivery.AddOrder(newOrder);
        }

        public void RemoveOrder(Order order)
        {
            Delivery.RemoveOrder(order);
        }


        public override string ToString()
        {
            return Id.ToString();
        }
    }
}
