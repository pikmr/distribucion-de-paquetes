﻿using DisPackages.Entities;

namespace DisPackages.Services
{
    public interface IGeoLocationService
    {
        public string GetDataLocation(ILocation location);
    }
}