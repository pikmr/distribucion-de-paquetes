﻿using DisPackages.Entities;
using System.Threading.Tasks;

namespace DisPackages.Services
{
    public  interface IVehicleRepository
    {
        Task<IVehicle> UpdateVehicleAsync(IVehicle value);
        Task<IVehicle> GetVehicleAsync(int idVehicle);
        Task<IVehicle> GetVehicleWithPackageAsync(int idVehicle);
    }
}