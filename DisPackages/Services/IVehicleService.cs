﻿using DisPackages.Api;
using DisPackages.Entities;
using System.Threading.Tasks;

namespace DisPackages.Services
{
    public interface IVehicleService
    {
        Task<ILocation> GetLocation(int IdVehicle);
        Task<IVehicle> InsertLocation(int IdVehicle, ILocation NewLocation);
        Task<IVehicle> UpdateLocation(int IdVehicle, ILocation NewLocation);
        Task<IVehicle> InsertOrder(int IdVehicle, Order NewOrder);
        Task<IVehicle> RemoveOrder(int IdVehicle, Order Order);
        Task<InfoOrder> GetInfoPackage(int IdPackage);

    }
}