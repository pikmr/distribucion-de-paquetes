﻿using DisPackages.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DisPackages.Services
{
    public class GoogleLocationService : IGeoLocationService
    {
        private readonly string _TOKEN;

        public GoogleLocationService()
        {
            _TOKEN = "_DEV_TOKEN";
        }

        public GoogleLocationService(IConfiguration config)
        {
            _TOKEN = config["Location:ServiceApiKey"];
        }
      
        public string GetDataLocation(ILocation location)
        {

            string url = $"https://maps.googleapis.com/maps/api/directions/json?origin={location.CoordinateX}+{location.CoordinateY}&key={_TOKEN}";
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            StreamReader reader = new StreamReader(data);
            // json-formatted string from maps api
            string responseFromServer = reader.ReadToEnd();
            response.Close();
            return responseFromServer;
        }
    }
}
