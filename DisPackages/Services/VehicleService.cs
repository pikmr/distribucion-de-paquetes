﻿using DisPackages.Api;
using DisPackages.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DisPackages.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _repository;

        public VehicleService(IVehicleRepository repository)
        {
            _repository = repository;
        }

        public Task<ILocation> GetLocation(int IdVehicle)
        {
            var vehicle = GetVehicleAsync(IdVehicle).Result;
            return Task.FromResult(vehicle.GetActualLocation());
        }

        public async Task<IVehicle> InsertLocation(int IdVehicle, ILocation NewLocation)
        {
            var vehicle = GetVehicleAsync(IdVehicle).Result;
            vehicle.AddLocation(NewLocation);
            return await _repository.UpdateVehicleAsync(vehicle);
        }

        public async Task<IVehicle> UpdateLocation(int IdVehicle, ILocation NewLocation)
        {
            var vehicle = GetVehicleAsync(IdVehicle).Result;
            vehicle.UpdateLocation(NewLocation);
            return await _repository.UpdateVehicleAsync(vehicle);
        }

        public async Task<IVehicle> InsertOrder(int IdVehicle, Order NewOrder)
        {
            var vehicle = GetVehicleAsync(IdVehicle).Result;
            vehicle.AddOrder(NewOrder);
            return await _repository.UpdateVehicleAsync(vehicle);
        }

        public async Task<IVehicle> RemoveOrder(int IdVehicle, Order Order)
        {
            var vehicle = GetVehicleAsync(IdVehicle).Result;
            vehicle.RemoveOrder(Order);
            return await _repository.UpdateVehicleAsync(vehicle);
        }

        private async Task<IVehicle> GetVehicleAsync(int IdVehicle)
        {
            var vehicle = await _repository.GetVehicleAsync(IdVehicle);
            if (vehicle == null)
                throw new ArgumentException("Vehicle not found");

            return vehicle;
        }

        public async Task<InfoOrder> GetInfoPackage(int IdPackage)
        {
            var vehicle = await _repository.GetVehicleWithPackageAsync(IdPackage);

            if (vehicle == null)
                throw new ArgumentException("Vehicle not found");

            InfoOrder IOrder = new InfoOrder()
            {
                ActualLocation = vehicle.GetActualLocation().GetDataLocation(new GoogleLocationService()),
                ActualVehicle = vehicle.ToString()
            };
            return IOrder;
        }


    }
}
