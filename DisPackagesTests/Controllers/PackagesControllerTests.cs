﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DisPackages.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using DisPackages.Api;
using Moq;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using DisPackages.Services;
using DisPackages.Entities;

namespace DisPackages.Controllers.Tests
{
    [TestClass()]
    public class PackagesControllerTests
    {
        string json_respond = "{\"results\":[{\"address_components\":[{\"long_name\":\"1600\",\"short_name\":\"1600\",\"types\":[\"street_number\"]},{\"long_name\":\"Murcia\",\"short_name\":\"MUR\",\"types\":[\"route\"]},{\"long_name\":\"Murcia\",\"short_name\":\"Murcia\",\"types\":[\"locality\",\"political\"]},{\"long_name\":\"Carretera de Alicante\",\"short_name\":\"Carretera de Alicante\",\"types\":[\"administrative_area_level_2\",\"political\"]},{\"long_name\":\"Murcia\",\"short_name\":\"MUR\",\"types\":[\"administrative_area_level_1\",\"political\"]},{\"long_name\":\"Spain\",\"short_name\":\"ES\",\"types\":[\"country\",\"political\"]},{\"long_name\":\"30007\",\"short_name\":\"30007\",\"types\":[\"postal_code\"]}],\"formatted_address\":\"30007 Carretera de Alicante, Murcia, MUR 30007, SPAIN\",\"geometry\":{\"location\":{\"lat\":37.4224764,\"lng\":-122.0842499},\"location_type\":\"ROOFTOP\",\"viewport\":{\"northeast\":{\"lat\":37.4238253802915,\"lng\":-122.0829009197085},\"southwest\":{\"lat\":37.4211274197085,\"lng\":-122.0855988802915}}},\"place_id\":\"ChIJ2eUgeAK6j4ARbn5u_wAGqWA\",\"plus_code\":{\"compound_code\":\"CWC8+W5 Carretera de Alicante, Murcia, MUR 30007, SPAIN\",\"global_code\":\"849VCWC8+W5\"},\"types\":[\"street_address\"]}],\"status\":\"OK\"}";

        [TestMethod()]
        public void GetInfoPackageTest()
        {

            var mock_iorder = new Mock<InfoOrder>();
            InfoOrder iorder_result = new InfoOrder() { ActualLocation = json_respond, ActualVehicle = "100" };

            mock_iorder.SetupGet(p => p.ActualLocation).Returns(json_respond).Verifiable();
            mock_iorder.SetupGet(p => p.ActualVehicle).Returns("100").Verifiable();
            Assert.AreEqual(json_respond, mock_iorder.Object.ActualLocation);
            Assert.AreEqual("100", mock_iorder.Object.ActualVehicle);
        }

        [TestMethod()]
        public void GetLocationTest()
        {
            Entities.Location location = new Entities.Location()
            {
                CoordinateX = 100,
                CoordinateY = 300,
                Sector = "Murcia"
            };

            Mock<VehicleService> service = new Mock<VehicleService>();
            service.Setup(s => s.GetLocation(10)).ReturnsAsync(location);
            var controller = new PackagesController(service.Object);
            var values = controller.GetLocation(10);
            Assert.AreEqual(values.Result, service.Object.GetLocation(10));
        }

        [TestMethod()]
        public void Controller_InsertLocationTest()
        {
            Entities.Location location = new Entities.Location()
            {
                CoordinateX = 100,
                CoordinateY = 300,
                Sector = "Murcia"
            };

            DisPackages.Entities.Vehicle vehicle =
                new Entities.Vehicle()
                {
                    Id = 10
                };

            Mock<VehicleService> service = new Mock<VehicleService>();
            service.Setup(s => s.InsertLocation(10, location)).ReturnsAsync(vehicle);
            var controller = new PackagesController(service.Object);
            var values = controller.AddLocation(10, location);
            Assert.AreEqual(values.Result, service.Object.InsertLocation(10, location));
        }

        [TestMethod()]
        public void Controller_UpdateLocationTest()
        {
            Entities.Location location = new Entities.Location()
            {
                CoordinateX = 100,
                CoordinateY = 300,
                Sector = "Murcia"
            };

            DisPackages.Entities.Vehicle vehicle =
                new Entities.Vehicle()
                {
                    Id = 10
                };

            Mock<VehicleService> service = new Mock<VehicleService>();
            service.Setup(s => s.UpdateLocation(10, location)).ReturnsAsync(vehicle);
            var controller = new PackagesController(service.Object);
            var values = controller.UpdateLocation(10, location);
            Assert.AreEqual(values.Result, service.Object.UpdateLocation(10, location));
        }

        [TestMethod()]
        public void Controller_GetInfoPackageTest()
        {
            var mock_iorder = new Mock<InfoOrder>();
            var json_respond = "{\"results\":[{\"address_components\":[{\"long_name\":\"1600\",\"short_name\":\"1600\",\"types\":[\"street_number\"]},{\"long_name\":\"Murcia\",\"short_name\":\"MUR\",\"types\":[\"route\"]},{\"long_name\":\"Murcia\",\"short_name\":\"Murcia\",\"types\":[\"locality\",\"political\"]},{\"long_name\":\"Carretera de Alicante\",\"short_name\":\"Carretera de Alicante\",\"types\":[\"administrative_area_level_2\",\"political\"]},{\"long_name\":\"Murcia\",\"short_name\":\"MUR\",\"types\":[\"administrative_area_level_1\",\"political\"]},{\"long_name\":\"Spain\",\"short_name\":\"ES\",\"types\":[\"country\",\"political\"]},{\"long_name\":\"30007\",\"short_name\":\"30007\",\"types\":[\"postal_code\"]}],\"formatted_address\":\"30007 Carretera de Alicante, Murcia, MUR 30007, SPAIN\",\"geometry\":{\"location\":{\"lat\":37.4224764,\"lng\":-122.0842499},\"location_type\":\"ROOFTOP\",\"viewport\":{\"northeast\":{\"lat\":37.4238253802915,\"lng\":-122.0829009197085},\"southwest\":{\"lat\":37.4211274197085,\"lng\":-122.0855988802915}}},\"place_id\":\"ChIJ2eUgeAK6j4ARbn5u_wAGqWA\",\"plus_code\":{\"compound_code\":\"CWC8+W5 Carretera de Alicante, Murcia, MUR 30007, SPAIN\",\"global_code\":\"849VCWC8+W5\"},\"types\":[\"street_address\"]}],\"status\":\"OK\"}";

            InfoOrder iorder_result = new InfoOrder() { ActualLocation = json_respond, ActualVehicle = "10" };

            Mock<VehicleService> service = new Mock<VehicleService>();
            service.Setup(s => s.GetInfoPackage(10)).ReturnsAsync(iorder_result);
            var controller = new PackagesController(service.Object);
            var values = controller.GetInfoPackage(10);
            Assert.AreEqual(values.Result, service.Object.GetInfoPackage(10));
        }

        [TestMethod()]
        public void AddOrderTest()
        {
            Order resultOrder = new Order() { Id = 1000 };
            DisPackages.Entities.Vehicle vehicle =
            new Entities.Vehicle()
            {
                Id = 10,
                Delivery = new Delivery() { Id = 100 }
            };
            vehicle.AddOrder(resultOrder);

            Mock<VehicleService> service = new Mock<VehicleService>();
            service.Setup(s => s.InsertOrder(10, resultOrder)).ReturnsAsync(vehicle);
            //UpdateLocation(10, location)).ReturnsAsync(vehicle);
            var controller = new PackagesController(service.Object);
            var values = controller.AddOrder(10, resultOrder);
            Assert.AreEqual(values.Result, service.Object.InsertOrder(10, resultOrder));
        }

        [TestMethod()]
        public void DeleteOrderTest()
        {
            Order resultOrder = new Order() { Id = 1000 };
            DisPackages.Entities.Vehicle vehicle =
            new Entities.Vehicle()
            {
                Id = 10,
                Delivery = new Delivery() { Id = 100 }
            };
            vehicle.AddOrder(resultOrder);

            Mock<VehicleService> service = new Mock<VehicleService>();
            service.Setup(s => s.RemoveOrder(10, resultOrder)).ReturnsAsync(vehicle);
            var controller = new PackagesController(service.Object);
            var values = controller.DeleteOrder(10, resultOrder);
            Assert.AreEqual(values.Result, service.Object.RemoveOrder(10, resultOrder));
        }
    }
}