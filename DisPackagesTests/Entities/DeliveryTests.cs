﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DisPackages.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using DisPackages.Services;
using System.Linq;
using Moq;

namespace DisPackages.Entities.Tests
{
    [TestClass()]
    public class DeliveryTests
    {
        [TestMethod()]
        public void AddOrderTest()
        {
            DisPackages.Entities.Vehicle vehicle =
                new Entities.Vehicle()
                {
                    Id = 1,
                    Delivery=new Delivery() { Id=100}
                };
            vehicle.Delivery.AddOrder( new Order() { Id = 1000});
            
            var repo = new Mock<IVehicleRepository>();
            repo.Setup(x=>x.UpdateVehicleAsync(vehicle)).ReturnsAsync(()=> vehicle).Verifiable();
            repo.Verify(x => x.UpdateVehicleAsync(vehicle));
        }

        [TestMethod()]
        public void RemoveOrderTest()
        {
            var mockOrder = new Order() { Id = 1000 };
            DisPackages.Entities.Vehicle vehicle =
            new Entities.Vehicle()
            {
                Id = 1,
                Delivery = new Delivery() { Id = 100 }
            };
            vehicle.Delivery.AddOrder(mockOrder);
            vehicle.Delivery.RemoveOrder(mockOrder);

            var repo = new Mock<IVehicleRepository>();
            repo.Setup(x => x.UpdateVehicleAsync(vehicle)).ReturnsAsync(() => vehicle);
            repo.Verify(x => x.UpdateVehicleAsync(vehicle));
            Assert.IsFalse(vehicle.Delivery.GetCopyOrders().Any());
        }
    }
}