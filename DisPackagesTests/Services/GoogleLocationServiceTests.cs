﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DisPackages.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;

namespace DisPackages.Services.Tests
{
    [TestClass()]
    public class GoogleLocationServiceTests
    {
        /// <summary>
        /// Json de respuesta para la localización.
        /// </summary>
        string json_respond = "{\"results\":[{\"address_components\":[{\"long_name\":\"1600\",\"short_name\":\"1600\",\"types\":[\"street_number\"]},{\"long_name\":\"Murcia\",\"short_name\":\"MUR\",\"types\":[\"route\"]},{\"long_name\":\"Murcia\",\"short_name\":\"Murcia\",\"types\":[\"locality\",\"political\"]},{\"long_name\":\"Carretera de Alicante\",\"short_name\":\"Carretera de Alicante\",\"types\":[\"administrative_area_level_2\",\"political\"]},{\"long_name\":\"Murcia\",\"short_name\":\"MUR\",\"types\":[\"administrative_area_level_1\",\"political\"]},{\"long_name\":\"Spain\",\"short_name\":\"ES\",\"types\":[\"country\",\"political\"]},{\"long_name\":\"30007\",\"short_name\":\"30007\",\"types\":[\"postal_code\"]}],\"formatted_address\":\"30007 Carretera de Alicante, Murcia, MUR 30007, SPAIN\",\"geometry\":{\"location\":{\"lat\":37.4224764,\"lng\":-122.0842499},\"location_type\":\"ROOFTOP\",\"viewport\":{\"northeast\":{\"lat\":37.4238253802915,\"lng\":-122.0829009197085},\"southwest\":{\"lat\":37.4211274197085,\"lng\":-122.0855988802915}}},\"place_id\":\"ChIJ2eUgeAK6j4ARbn5u_wAGqWA\",\"plus_code\":{\"compound_code\":\"CWC8+W5 Carretera de Alicante, Murcia, MUR 30007, SPAIN\",\"global_code\":\"849VCWC8+W5\"},\"types\":[\"street_address\"]}],\"status\":\"OK\"}";

        [TestMethod()]
        public void GetDataLocationTest()
        {
            Entities.Location location = new Entities.Location()
            {
                CoordinateX = 100,
                CoordinateY = 300,
                Sector = "Murcia"
            };

            var repo = new Mock<GoogleLocationService>();
            repo.Setup(x => x.GetDataLocation(location)).Returns(() => json_respond).Verifiable();
            repo.Verify(x => x.GetDataLocation(location));
        }
    }
}