﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DisPackages.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using System.Threading.Tasks;

namespace DisPackages.Services.Tests
{
    [TestClass()]
    public class VehicleServiceTests
    {
  
        [TestMethod()]
        public void GetLocationTest()
        {
            DisPackages.Entities.Vehicle vehicle = 
                new Entities.Vehicle() { 
                    Id = 1        
                };

            
            var repo = new Mock<IVehicleRepository>();
            repo.Setup(x => x.GetVehicleAsync(1)).ReturnsAsync(() => vehicle).Verifiable();
            repo.Verify(x => x.GetVehicleAsync(1));
        }


        [TestMethod()]
        public void UpdateLocationTest()
        {
            DisPackages.Entities.Vehicle vehicle =
            new Entities.Vehicle()
            {
                Id = 4
            };

            var repo = new Mock<IVehicleRepository>();
            repo.Setup(x => x.UpdateVehicleAsync(new Entities.Vehicle() { Id = 4})).ReturnsAsync(() => vehicle).Verifiable();
            repo.Verify(x => x.UpdateVehicleAsync(new Entities.Vehicle() { Id = 4 }));
        }
    }
}